---
layout: home
bootstrap: true

# title
postnav_title: "The Zeitgeist Movement"

# second title
postnav_subtitle: "Shire Chapter"

# home page header image
header_image: "/assets/img/home-bg.jpg"

# if header height is reduced
slim_header: true
---
